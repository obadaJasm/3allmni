package com.example.a3allmni;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class LongVowelChooserActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv1,tv2,tv3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_long_vowel);
    tv1=findViewById(R.id.chooser_firsttv);
    tv2=findViewById(R.id.chooser_secondtv);
    tv3=findViewById(R.id.chooser_thirdtv);
    tv1.setOnClickListener(this);
    tv2.setOnClickListener(this);
    tv3.setOnClickListener(this);




    }

    @Override
    public void onClick(View v) {

        if(v.getId() ==tv1.getId()){
           SendData(0);
        }else if (v.getId() ==tv2.getId()) {
            SendData(1);
        }else if (v.getId() ==tv3.getId()) {
            SendData(2);
        }
        }

    public void SendData(int a){
        Intent i = new Intent(LongVowelChooserActivity.this,LongVowelActivity.class);
        i.putExtra("ID",a);
        startActivity(i);
    }


}

