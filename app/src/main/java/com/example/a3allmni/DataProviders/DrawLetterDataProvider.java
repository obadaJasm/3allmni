package com.example.a3allmni.DataProviders;

import com.example.a3allmni.R;
import com.example.a3allmni.model.Word;
import com.example.a3allmni.model.letter;

public class DrawLetterDataProvider {

    Word[] word;

    public void setWord(Word[] word) {
        this.word = word;
    }

    public Word[] getWord() {
        return word;
    }

    public static Word[] getWords(){
        return  DATA;
    }
    /*
     * أ، ب، ت، ث، ج، ح، خ، د، ذ، ر، ز، س، ش، ص، ض، ط، ظ، ع، غ، ف، ق، ك، ل، م، ن، هـ، و، ي
     * */
    public static final Word a = new Word("أ", R.drawable.galef);
    public static final Word b = new Word("ب", R.drawable.gbaa);
    public static final Word c = new Word("ت", R.drawable.gtaa);
    public static final Word d= new Word("ث", R.drawable.gthaa);
    public static final Word e= new Word("ج", R.drawable.ggeem);
    public static final Word f= new Word("ح", R.drawable.ghaa);
    public static final Word g= new Word("خ", R.drawable.gkhaa);
    public static final Word h= new Word("د", R.drawable.gdaal);
    public static final Word i= new Word("ذ", R.drawable.gtha);
    public static final Word aaa= new Word("ر", R.drawable.gra);
    public static final Word aa= new Word("ز", R.drawable.gza);
    public static final Word ha= new Word("س", R.drawable.gsen);
    public static final Word haaa= new Word("ش", R.drawable.gshen);
    public static final Word haa= new Word("ص", R.drawable.gsad);
    public static final Word haaaaa= new Word("ض", R.drawable.gdad);
    public static final Word hb= new Word("ط", R.drawable.gtaa);
    public static final Word hbb= new Word("ظ", R.drawable.gtthaaaa);
    public static final Word hbbb= new Word("ع", R.drawable.gaeen);
    public static final Word hbbbb= new Word("غ", R.drawable.gghen);
    public static final Word hd= new Word("ف", R.drawable.gfa);
    public static final Word hdd= new Word("ق", R.drawable.gkafff);
    public static final Word hddd= new Word("ك", R.drawable.gkaf);
    public static final Word hdddd= new Word("ل", R.drawable.glam);
    public static final Word hq= new Word("م", R.drawable.gmem);
    public static final Word hqq= new Word("ن", R.drawable.gnon);
    public static final Word hqqq= new Word("ه", R.drawable.gha);
    public static final Word hw= new Word("و", R.drawable.gwaw);
    public static final Word hww= new Word("ي", R.drawable.gea);


    public static final Word[] DATA={a,b,c,d,e,f,g,h,i,aaa,aa,ha,haaa,haa,haaaaa,hb,hbb,hbb,hbbb,hbbbb,hd,hdd,hddd,hdddd,hq,hqq,hqqq,hw,hww
    };
}
