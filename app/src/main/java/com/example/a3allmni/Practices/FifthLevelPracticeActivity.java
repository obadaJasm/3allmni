package com.example.a3allmni.Practices;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a3allmni.DataProviders.lettersProvider;
import com.example.a3allmni.R;
import com.example.a3allmni.model.letter;

import java.util.Random;

public class FifthLevelPracticeActivity extends AppCompatActivity {
    private letter[] DATA;
    private ImageView iv;
    private TextView tv;
    private int randomNumber, randomImageIndex;
    private letter selectedLetter;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifth_level_practice);

        iv = findViewById(R.id.practice_fifthlevel_iv);
        tv = findViewById(R.id.practice_fifthlevel_tv);

        DATA = lettersProvider.DATA;

        generateNewQuestion();
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                generateNewQuestion();
            }
        });


    }

    private void generateNewQuestion() {
        Random random = new Random();
        randomNumber = random.nextInt(28);
        randomImageIndex = random.nextInt(2);

        selectedLetter = DATA[randomNumber];

        iv.setBackgroundResource(selectedLetter.getWords().get(randomImageIndex).getImage());
        tv.setText(selectedLetter.getWords().get(randomImageIndex).getTitle());

    }
}
