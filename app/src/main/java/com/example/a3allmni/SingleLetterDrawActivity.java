package com.example.a3allmni;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;

import pl.droidsonroids.gif.GifImageView;

public class SingleLetterDrawActivity extends AppCompatActivity {
    GifImageView gifImageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_letter_draw);

        gifImageView = findViewById(R.id.singleletterdraw_iv);
        setTitle(" ");
        int ImageRes= getIntent().getExtras().getInt("IMAGE_RES");
        gifImageView.setImageResource(ImageRes);


    }
}
