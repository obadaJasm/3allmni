package com.example.a3allmni;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.a3allmni.Adapters.LongVowelRecylerViewAdapter;
import com.example.a3allmni.DataProviders.LongVowelDataProvider;
import com.example.a3allmni.DataProviders.lettersProvider;
import com.example.a3allmni.model.letter;

public class LongVowelActivity extends AppCompatActivity implements LongVowelRecylerViewAdapter.ItemClickListener {
public static final String TAG = "TEST";

LongVowelRecylerViewAdapter adapter;
   LongVowelDataProvider[] DATAa;
    RecyclerView recyclerView;
    int helper;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_longvowel);
        helper = getIntent().getIntExtra("ID",0);
        DATAa= LongVowelDataProvider.getDATA();


          recyclerView = findViewById(R.id.long_recycler);
            int numberOfColumns = 2;
            GridLayoutManager myGridlayoutManager;
            myGridlayoutManager= new GridLayoutManager(this, numberOfColumns);
            //myGridlayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(myGridlayoutManager);


        adapter = new LongVowelRecylerViewAdapter( DATAa[helper]);
        adapter.setClickListener(this);

        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onItemClick(View view, int position) {

        Log.d(TAG, "onItemClick: "+position);
                //implement sound here
        mediaPlayer = MediaPlayer.create(LongVowelActivity.this,DATAa[helper].getExamples()[position].getImage());
        mediaPlayer.start();
        Log.d(TAG, "onItemClick2: "+position);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mediaPlayer!=null){
            mediaPlayer.stop();
            mediaPlayer = null;
        }


    }
}

